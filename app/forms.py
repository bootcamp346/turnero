from django import forms
from .models import Condicion, Cliente, Servicio

class SeleccionarCliente(forms.Form):
    documento = forms.CharField(max_length=50, required=True, label="Número de Documento")

    def clean_documento(self):
        documento = self.cleaned_data['documento']
        try:
            cliente = Cliente.objects.get(documento=documento)
        except Cliente.DoesNotExist:
            raise forms.ValidationError("No se encontró ningún Cliente con el Número de Documento: "+documento)
        return cliente

    def __init__(self, *args, **kwargs):
        super(SeleccionarCliente, self).__init__(*args, **kwargs)
        self.fields['documento'].widget.attrs.update({'autofocus': 'autofocus'})


class SeleccionarCondicion(forms.Form):
    cliente_id = forms.CharField(max_length=50, required=False, label="", widget=forms.HiddenInput())
    cliente_nombre = forms.CharField(max_length=50, required=False, disabled=True, label="Cliente")
    condicion_id = forms.ModelChoiceField(queryset=Condicion.objects.all(), required=True, label="Condición del Cliente")
    servicio_id = forms.ModelChoiceField(queryset=Servicio.objects.all(), required=True, label="Seleccione el Servicio deseado")

    def __init__(self, *args, **kwargs):
        super(SeleccionarCondicion, self).__init__(*args, **kwargs)
        self.fields['servicio_id'].widget.attrs.update({'autofocus': 'autofocus'})