from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from .models import *
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

admin.site.register(Servicio)
admin.site.register(BocaAtencion)
admin.site.register(Condicion)


#Clientes
class ClienteAdmin(admin.ModelAdmin):
    list_display = ['documento', '__str__', 'condicion', 'genero', 'telefono']
admin.site.register(Cliente, ClienteAdmin)


#Turnos
class TurnoAdmin(admin.ModelAdmin):
    list_display = ['cliente', 'boca_atencion', 'condicion', 'fecha_hora_llegada', 'fecha_hora_llamada', 'estado']
admin.site.register(Turno, TurnoAdmin)


#UsuarioBocaAtencion
class UsarioBocaAtencionInLine(admin.StackedInline):
    model = UsarioBocaAtencion
    can_delete = False
    verbose_name = "Boca de Atención"
    verbose_name_plural = "Boca de Atención"


# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = [UsarioBocaAtencionInLine]
admin.site.unregister(User)
admin.site.register(User, UserAdmin)